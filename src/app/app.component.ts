import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test-task-frontend';
  numbers = [1, 2, 3, 4];
  shownCharts = 1;
  startDate;
  endDate;

  constructor() {
    this.startDate = new Date();
    this.startDate.setMonth(0);
    this.endDate = new Date();
    this.endDate.setMonth(11);
  }

  getShownChartsCount() {
    return new Array(this.shownCharts);
  }


  // Handler to trigger after month is selected
  setMonthHandler(isStartDate, date, datePicker) {
    if (isStartDate) {
      this.startDate = date;
      // set day of month to 1, not relevant for this demo
      this.startDate.setDate(1);
    } else {
      this.endDate = date;
      // set day of month to 1, not relevant for this demo
      this.endDate.setDate(1);
    }
    datePicker.close();
  }
}
