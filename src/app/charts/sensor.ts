export class Sensor {
  id: number;
  name: string;
  color: string;
  checked: boolean;
  dataSet:
    {
      data: number[], label: string, fill: boolean
    };
}
