import {Component, Input, OnInit} from '@angular/core';
import {Sensor} from './sensor';
import {ChartDataSets} from 'chart.js';
import {Color, Label} from 'ng2-charts';

/* tslint:disable */

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})

export class ChartsComponent implements OnInit {
  @Input() chartNum;
  temperatureValues = [10, 15, 20, 22, 23, 29, 30, 26, 23, 15, 10, 5];
  humidityValues = [65, 59, 80, 81, 56, 55, 40, 34, 45, 20, 60, 20];
  lightValues = [40, 20, 40, 30, 39, 30, 70, 80, 20, 13, 40, 60];
  monthValues = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'];

  startDate;
  endDate;

  chartTypes: string[] = ['line', 'bar'];


  sensorList: Sensor[] = [
    {
      id: 1,
      name: 'temperature',
      color: 'red',
      checked: true,
      dataSet: {data: [10, 15, 20, 22, 23, 29, 30, 26, 23, 15, 10, 5], label: 'temperatur', fill: false}
    },
    {
      id: 2,
      name: 'humidity',
      color: 'blue',
      checked: false,

      dataSet: {data: [65, 59, 80, 81, 56, 55, 40, 34, 45, 20, 60, 20], label: 'humidity', fill: false}
    },
    {
      id: 3,
      name: 'light',
      color: 'yellow',
      checked: false,

      dataSet: {data: [40, 20, 40, 30, 39, 30, 70, 80, 20, 13, 40, 60], label: 'light', fill: false}
    },
  ];


  lineChartData: ChartDataSets[] = [];
  lineChartLabels: Label[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'];
  lineChartOptions: { responsive: boolean } = {
    responsive: false
  };
  lineChartColors: Color[] = [
    {
      borderColor: 'red',
      backgroundColor: 'red',
    },
  ];
  lineChartLegend = true;
  lineChartType = 'line';
  lineChartPlugins = [];
  color: any;

  constructor() {
    // Defaultdataset temperature
    this.lineChartData.push(this.sensorList[0].dataSet);


  }

  ngOnInit() {

  }

  // toggle type of chart
  setChartType(chartType) {
    this.lineChartType = chartType;
  }

  // toggle Sensor data to show in chart
  setChartSensor(sensor) {
    // if checkbox is true, push sensordata
    if (sensor.checked) {
      this.lineChartData.push(sensor.dataSet);
      this.lineChartColors.push({
        borderColor: sensor.color,
        backgroundColor: sensor.color,
      });

    } else {
      // else delete sensordata
      let index = this.lineChartData.indexOf(sensor.dataSet);
      this.lineChartData.splice(index, 1);
      this.lineChartColors.splice(index, 1);

    }

  }

  // Set the color of the dataset inside the chart
  setChartColor(sensor) {
    let index = this.lineChartData.indexOf(sensor.dataSet);
    //check index of sensor dataset in chart
    if (index >= 0) {
      this.lineChartColors[index].borderColor = sensor.color;
      this.lineChartColors[index].backgroundColor = sensor.color;
    }
  }


  @Input()
  set startMonth(date) {
    if (date) {
      this.startDate = date.getMonth();
    } else {
      //default startdate is 0 = January
      this.startDate = 0;
    }

    this.reduceValues();
  }

  @Input()
  set endMonth(date) {
    if (date) {
      this.endDate = date.getMonth() + 1;
    } else {
      // default enddate is 12 = December
      this.endDate = 12;
    }

    this.reduceValues();
  }

  // Reduce all datasets to the values between startdate and enddate
  reduceValues() {
    // temperature data list
    this.sensorList[0].dataSet.data = this.temperatureValues.slice(this.startDate, this.endDate);
    // humidity data list
    this.sensorList[1].dataSet.data = this.humidityValues.slice(this.startDate, this.endDate);
    // light data list
    this.sensorList[2].dataSet.data = this.lightValues.slice(this.startDate, this.endDate);

    // x-axis labels
    this.lineChartLabels = this.monthValues.slice(this.startDate, this.endDate);


  }
}
